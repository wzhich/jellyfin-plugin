﻿using Jellyfin.Plugin.JavMetadata.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Site.Avsox.Configuration
{
    public enum AvsoxLanguage
    {
        /// <summary>
        /// 英语
        /// </summary>
        En,

        /// <summary>
        /// 日语
        /// </summary>
        Ja,

        /// <summary>
        /// 繁体中文
        /// </summary>
        Tw,

        /// <summary>
        /// 简体中文
        /// </summary>
        Cn
    }

    public class AvsoxConfiguration : DefaultConfiguration
    {
        /// <summary>
        /// 语言
        /// </summary>
        public AvsoxLanguage Language { get; set; }

        /// <summary>
        /// 识别码和标题正则
        /// </summary>
        public string TitlePattern { get; set; }

        /// <summary>
        /// 预览图列表正则
        /// </summary>
        public string ScreenshotListPattern { get; set; }

        /// <summary>
        /// 预览图正则
        /// </summary>
        public string ScreenshotPattern { get; set; }

        public AvsoxConfiguration()
        {

            // set default options here
            ProviderId = $"Avsox Id";
            ProviderName = "AVSOX";
            Language = AvsoxLanguage.Cn;
            Domain = "avsox.monster";
            // SearchResultPattern = @"/movie/([\d\w]+)";
            SearchResultPattern = @"movie/(?<id>.*?)""[\w\W]*?src=""(?<poster>.*?)""\stitle=""(?<title>.*?)""[\w\W]*?<br><date>(?<avid>.*?)</date>\s/\s<date>(?<date>.*?)</";
            CoverPattern = @"bigImage""\shref=""(?<fanart>.*?)""";
            TitlePattern = @"<h3>(?<title>.*?)</h3>";
            ReleaseDatePattern = @"发行时间:</span>\s(?<date>.*?)</p>";
            DurationPattern = @"长度:</span>\s(?<duration>\d+)";
            DirectorListPattern = @"导演:</span>\s(?<directors>.*?)</p>";
            DirectorPattern = "href=\"(?<url>.*?)\">(?<name>.*?)<";
            StudioListPattern = @"制作商:\s</p>\s*<p>(?<studios>.*?)\s*</p>";
            StudioPattern = "href=\"(?<url>.*?)\">(?<name>.*?)<";
            LabelListPattern = @"发行商:\s</p>\s*<p>(?<labels>.*?)\s*</p>";
            LabelPattern = "href=\"(?<url>.*?)\">(?<name>.*?)<";
            CollectionListPattern = @"系列:</p>\s*<p>(?<series>.*?)\s*</p>";
            CollectionPattern = "href=\"(?<url>.*?)\">(?<name>.*?)<";
            GenreListPattern = @"类别:</p>\s*<p>(?<genres>.*?)\s*</p>";
            GenrePattern = "href=\"(?<url>.*?)\">(?<name>.*?)<";
            ActressListPattern = @"avatar-waterfall"">\s*(?<actresses>[\w\W]*?)\s*</div>\s*<div";
            ActressPattern = @"href=""(?<url>.*?)""[\w\W]*?src=""(?<photo>.*?)""[\w\W]*?<span>(?<name>.*?)<";
            ScreenshotListPattern = @"sample-waterfall"">\s*(?<screenshots>[\w\W]*?)\s*</div>\s*<div";
            ScreenshotPattern = @"href=""(?<url>.*?)""\stitle=""(?<name>.*?)""";
        }
    }
}
