﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class PersonAvatar
    {
        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }
    }
}
